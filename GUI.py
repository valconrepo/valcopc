import dash_html_components as html
import dash_core_components as dcc
import dash
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State, ALL
import json
import pandas as pd
from PyOv import PyOv
from PyOPC import OvationVariableList
import base64
import io
import time
import os


def update_table(df):
    rows = [html.Tr([html.Td(row [0]), html.Td(row [1]), html.Td(row [2]), html.Td(row [3]), html.Td(row [4]), html.Td(
        [
            dbc.Button(id={'type': 'delete-point-button', 'point-name': row [0]})
        ]
    )]) for index, row in df.iterrows()]
    return rows


# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

UPLOAD_STYLE = {
    'width': '100%',
    'height': '60px',
    'lineHeight': '60px',
    'borderWidth': '1px',
    'borderStyle': 'dashed',
    'borderRadius': '5px',
    'textAlign': 'center'
}

OvVarList = OvationVariableList()

external_stylesheets = [dbc.themes.BOOTSTRAP]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

rows = []
indexes = {}

df = pd.DataFrame(columns=['name', 'type', 'unit_name', 'network_name', 'r_w'])

table_header = [
    html.Thead(html.Tr(
        [html.Th('Point Name'), html.Th('Unit name'), html.Th('Network name'),
         html.Th('Point Type'), html.Th('R/W'), html.Th('Delete')]))
]

table = dbc.Table(table_header, bordered=True, id='main-table')

form = dbc.Row(
    [
        dbc.Col(
            [
                dbc.Row(
                    [
                        dbc.Col(
                            [
                                dbc.Input(placeholder="please enter  point name", id='point-name-input',
                                          className='d-inline'),
                            ], width=6
                        ),
                        dbc.Col(
                            [
                                dbc.InputGroup(
                                    [
                                        dbc.InputGroupAddon('.', addon_type='append'),
                                        dbc.Input(placeholder="please enter unit name", id='unit-name-input',
                                                  className='d-inline'),
                                    ]
                                ),

                            ], width=3
                        ),
                        dbc.Col(
                            [
                                dbc.InputGroup(
                                    [
                                        dbc.InputGroupAddon('@', addon_type='append'),
                                        dbc.Input(placeholder="please enter network name", id='network-name-input',
                                                  className='d-inline'),
                                    ]
                                ),
                            ], width=3
                        )

                    ]
                ),

            ], width=9
        ),
        dbc.Col(
            [
                dbc.Select(id='point-type-input',
                           options=[
                               {'label': 'LA', 'value': 'LA'},
                               {'label': 'LD', 'value': 'LD'}
                           ], value='LA')
            ], width=1
        ),
        dbc.Col(
            [
                dbc.Select(id='point-rw',
                           options=[
                               {'label': 'Read', 'value': 'R'},
                               {'label': 'Write', 'value': 'W'}
                           ], value='R')
            ], width=1
        ),
        dbc.Col(
            [
                dbc.Button("Submit", color="primary", id='submit-button'),
            ], width=1
        )
    ], className='mt-3'
)

upload = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.Upload([
                            'Drag and Drop or ',
                            html.A('Select a File')
                        ], style=UPLOAD_STYLE, id='upload')
                    ]
                )
            ], className='mt-3'
        ),
        dbc.Row([
            dbc.Col(
                [
                    dbc.Label('Upload type', className='mr-2'),
                    dbc.RadioItems(
                        options=[
                            {'label': 'Append', 'value': 'Append'},
                            {'label': 'Overwrite', 'value': 'Overwrite'},
                        ],
                        value='Append',
                        id='upload-type-selector',
                        inline=True,
                        className='d-inline'
                    )
                ], className='mt-3'
            ),
            dbc.Col(
                [
                    dbc.Button('Download configuration', id='download-config', className='mr-2'),
                    html.Div(id='download-message', className='d-inline')
                ], className='mt-3'
            )
        ])
    ],
)

sidebar = html.Div(
    [
        html.H2("ValcOPC", className="display-4"),
        html.Hr(),
        html.P(
            "Valcon simple OPC client", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Configuration", href="/configuration", id="page-1-link"),
                dbc.NavLink("Point list", href="/pointlist", id="page-2-link"),
            ],
            vertical=True,
            pills=True,
        ),
    ], style=SIDEBAR_STYLE, )

config = html.Div([
    dbc.Row([
        dbc.Col([
            dbc.ButtonGroup([
                dbc.Button('Start', id="start-button"),
                dbc.Button('Stop', id='stop-button')
            ])
        ], width=2),
        dbc.Col([
            dbc.Button('Save', id='save-button')
        ]),
        dbc.Col([
            dbc.Alert(id='ov-connect-fb', is_open=False, fade=True)
        ], width=3),
        dbc.Col([
            dbc.Alert("Stopped", color="warning", id='OPC-status')
        ], width=5)
    ]),
    dbc.Row([
        dbc.Col([
            dbc.Label('Endpoint'),
            dbc.Input(id='opc-endpoint', value=OvVarList.Endpoint, className="mb-3"),
        ]),
        dbc.Col([
            dbc.Label('Scan Rate'),
            dbc.Input(id='scan-rate-input', value=OvVarList.ScanRate)
        ])
    ]),
])

modals = dbc.Modal(
    [
        dbc.ModalHeader("Cannot add point"),
        dbc.ModalBody("Unable to find SID therefore point will not be added"),
        dbc.ModalFooter(
            dbc.Button("Close", id="close", className="ml-auto")
        ),
    ],
    id="modal-cannot add point",
),

content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.callback(
    Output('main-table', 'children'),
    [Input('submit-button', 'n_clicks'), Input({'type': 'delete-point-button', 'point-name': ALL}, 'n_clicks'),
     Input('upload', 'contents')],
    [State('point-name-input', 'value'), State('point-name-input', "valid"), State('point-type-input', 'value'),
     State('point-rw', 'value'), State('upload-type-selector', 'value'), State('network-name-input', 'value'),
     State('unit-name-input', 'value')])
def add_new_point(click, click2, file_continent, point_name, valid, point_type, read_write, upload_type, network_name,
                  unit_name):
    global df, rows
    ctx = dash.callback_context
    button_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    if 'submit-button' == button_id:
        if point_name is not None and valid:
            df.loc [len(df)] = [point_name, unit_name, network_name, point_type, read_write]
            OvVarList.append_item([point_name + '.' + unit_name + '@' + network_name, point_type, read_write])
            rows = update_table(df)
    elif 'delete-point-button' in button_id:
        button_id_dict = json.loads(button_id)
        point_name = button_id_dict ['point-name']
        df = df [df.name != point_name]
        df.reset_index(drop=True, inplace=True)
        OvVarList.delete_item(point_name)
        rows = update_table(df)
    elif 'upload' in button_id:
        content_type, content_string = file_continent.split(',')
        decoded = base64.b64decode(content_string)
        try:
            df_new = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
            if upload_type == 'Overwrite':
                OvVarList.clear_lists()
                df = df_new
            else:
                df = df.append(df_new, ignore_index=True)
            OvVarList.append_list(df)
            rows = update_table(df)
        except Exception as e:
            print(e)
            raise PreventUpdate

    table_body = [html.Tbody(rows)]
    return table_header + table_body


@app.callback(
    [Output('point-name-input', "valid"), Output('point-name-input', "invalid")],
    [Input('point-name-input', 'value')])
def check_validity(value):
    valid = False
    if "@" not in value:
        valid = True
    return valid, not valid


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname in ["/", "/configuration"] or pathname == "/":
        return config
    elif pathname == "/pointlist":
        return [table, form, upload]
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


@app.callback(
    [Output('ov-connect-fb', 'children'), Output('ov-connect-fb', 'color'), Output('ov-connect-fb', 'is_open')],
    [Input('save-button', 'n_clicks')],
    [State('opc-endpoint', 'value'), State('scan-rate-input', 'value')])
def connect_to_ovation(clicks, endpoint_value, scanrate_value):
    ctx = dash.callback_context
    button_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    color = None
    message = None
    if button_id == 'save-button':
        if endpoint_value is not None and scanrate_value is not None:
            OvVarList.update_endpoint(endpoint_value)
            OvVarList.update_scanrate(scanrate_value)
        message = 'Successfully saved configuration!'
        color = 'success'
    return message, color, True


@app.callback([Output('OPC-status', 'children'), Output('OPC-status', 'color')],
              [Input("start-button", 'n_clicks'), Input("stop-button", 'n_clicks')])
def opc_server(clicks_start, clicks_stop):
    ctx = dash.callback_context
    button_id = ctx.triggered [0] ['prop_id'].split('.') [0]
    if button_id == 'start-button':
        OvVarList.start_update_objects()
        time.sleep(1)

    if button_id == 'stop-button':
        OvVarList.stop_update_objects()
        time.sleep(1)

    if OvVarList.update:
        message = 'OPC server started!'
        color = 'success'
    else:
        message = 'OPC server stopped!'
        color = 'warning'

    return message, color


@app.callback(Output('download-message', 'children'),
              [Input('download-config', 'n_clicks')])
def download_config(clicks):
    if clicks is None:
        raise PreventUpdate
    try:
        df.to_csv('export.csv', index=False)
        return dbc.Alert("Successfully saved point list!", color='success', className='d-inline')
    except Exception as e:
        return dbc.Alert(f'Cannot save configuration because exception occurred! Exception {e}', color='danger',
                         className='d-inline')


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')
