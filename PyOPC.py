from PyOv import PyOv
from PyOv import OvationDataItem
import sys
import configparser
import logging

sys.path.insert(0, "..")
import time
import csv
import threading

from opcua import ua, Server


def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()

    return wrapper


def paced(sleep_time):
    def wrap(fn):
        def wrapper(*args, **kwargs):
            start_time = time.perf_counter()
            fn(*args, **kwargs)
            end_time = time.perf_counter()
            run_time = end_time - start_time
            time.sleep(sleep_time - run_time)

        return wrapper

    return wrap


class OPCObject:

    def __init__(self, OvData, OPC_server):
        self.OvData = OvData
        self.OPCDataObject = OPC_server.objects.add_object(OPC_server.idx, OvData.pointName)
        self.OPCVarValue = self.OPCDataObject.add_variable(OPC_server.idx, "Value", OvData.value)
        self.OPCVarTime = self.OPCDataObject.add_variable(OPC_server.idx, "Time", OvData.time)
        self.OPCVarSID = self.OPCDataObject.add_variable(OPC_server.idx, "sid", OvData.sid)
        self.OPCVarStatus = self.OPCDataObject.add_variable(OPC_server.idx, "status", OvData.status)
        self.OPCVarValue.set_writable()

    def __del__(self):
        self.OPCDataObject.delete()

    def read(self, OPC_server):
        OPC_server.OvationGateway.GetValueAdv(self.OvData)
        self.OPCVarValue.set_value(self.OvData.value)
        self.OPCVarTime.set_value(self.OvData.time)
        self.OPCVarStatus.set_value(self.OvData.status)

    def write(self, OPC_server):
        self.OvData.value = self.OPCVarValue.get_value()
        if self.OvData.value is None:
            self.OvData.value = 0
        OPC_server.OvationGateway.PutValueAdv(self.OvData)


class OvationVariableList:
    server = None
    ScanRate = 1000
    Endpoint = 'opc.tcp://172.168.1.47:4840/freeopcua/server/'

    OvationGateway = None

    def __init__(self):
        self.update = False
        if OvationVariableList.server is None:
            OvationVariableList.server = Server()
            OvationVariableList.update_endpoint()
            uri = "http://examples.freeopcua.github.io"
            OvationVariableList.idx = OvationVariableList.server.register_namespace(uri)
            OvationVariableList.objects = OvationVariableList.server.get_objects_node()

        self.Read = []
        self.Write = []
        if OvationVariableList.OvationGateway is None:
            OvationVariableList.OvationGateway = PyOv()



    def append_list(self, df):
        for row in df:
            self.append_item(row)

    def append_item(self, row):
        OvData = OvationDataItem(pointName=row [0])
        if row [1] == "LA":
            OvData.type = 91
        if row [1] == "LD":
            OvData.type = 141
        if row [2] == "R":
            self.Read.append(OPCObject(OvData, self))
        if row [2] == "W":
            self.Write.append(OPCObject(OvData, self))

    def delete_item(self, point_name):
        self.Read = [item for item in self.Read if item.OvData.pointName.split('.')[0] != point_name]
        self.Write = [item for item in self.Write if item.OvData.pointName.split('.')[0] != point_name]

    def clear_lists(self):
        self.Read = []
        self.Write = []

    @threaded
    def start_update_objects(self):
        if not self.update:
            try:
                OvationVariableList.server.start()
                self.update = True
                while self.update:
                    time.sleep(self.ScanRate / 1000)
                    for OvObj in self.Read:
                        OvObj.read(self)
                    for OvObj in self.Write:
                        OvObj.write(self)
            finally:
                self.update = False
                OvationVariableList.server.stop()

    def stop_update_objects(self):
        if self.update:
            self.update = False

    @classmethod
    def update_endpoint(cls, endpoint=None):
        if endpoint is not None:
            cls.Endpoint = endpoint
        cls.server.set_endpoint(cls.Endpoint)

    @classmethod
    def update_scanrate(cls, ScanRate):
        cls.ScanRate = ScanRate


def setup_loggers():
    # set up loggers
    logging.basicConfig(filename='PyOPC.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s',
                        level=logging.WARNING)
    OPClogger = logging.getLogger("opcua")
    OPClogger.setLevel(logging.WARNING)


def initialise_gateway():
    # Initialise Ovation gateway
    OvationGateway = PyOv()
    OvationGateway.OvationConnect()
    return OvationGateway


if __name__ == "__main__":
    setup_loggers()

    # Open and read config file
    config = configparser.ConfigParser()
    try:
        with open('config.cnf', 'r') as configfile:
            config.read_file(configfile)
            Endpoint = config ['DEFAULT'] ['Endpoint']
            ScanRate = float(config ['DEFAULT'] ['ScanRate'])
    except:
        logging.exception("Unable to parse config.cnf file!")
        sys.exit()

    OvationGateway = initialise_gateway()
    if not OvationGateway.connected:
        logging.exception("Unable to connect to Ovation!")
        sys.exit()

    # Open and read variables file
    try:
        with open('variables.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', dialect='excel')
            OvationVariableListRead = []
            OvationVariableListWrite = []
            for row in csv_reader:
                OvData = OvationDataItem(pointName=row [1])
                if row [2] == "LA":
                    OvData.type = 91
                if row [2] == "LD":
                    OvData.type = 141
                OvationGateway.GetValueAdv(OvData)
                if row [3] == "R":
                    OvationVariableListRead.append(OvData)
                if row [3] == "W":
                    OvationVariableListWrite.append(OvData)
    except:
        logging.exception("Unable to parse variables.csv file!")
        sys.exit()

    # setup our server
    server = Server()
    server.set_endpoint(Endpoint)

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # Initiate OPC objects for each variable
    OvObjListRead = []
    OvObjListWrite = []

    for OvData in OvationVariableListRead:
        OvObjListRead.append(OPCObject(OvData))
    for OvData in OvationVariableListWrite:
        OvObjListWrite.append(OPCObject(OvData))

    try:
        # starting!
        server.start()
    except:
        logging.exception("OPC server cannot be started!")
    else:
        logging.info("OPC server started!")
        # update variables every scan rate
        try:
            while True:
                time.sleep(ScanRate / 1000)
                for OvObj in OvObjListRead:
                    OvObj.read(OvationGateway)
                for OvObj in OvObjListWrite:
                    OvObj.write(OvationGateway)
                logging.debug("Updated all objects!")

        finally:
            # close connection, remove subcsriptions, etc
            server.stop()
            logging.info("Server stopped!")
